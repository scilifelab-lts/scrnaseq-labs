Different methods for differential gene expression in scRNAseq data - part2
===========================================================================

Continuing with DE detection for the mouse embryonic dataset. Since many packages are loaded in this tutorial, many will get problems with maximum DLLs reached. So we suggest you contiune in a new R-session. Or modify your environmental variables to allow for more DLLs as explained in the [FAQ](FAQ.md).

In this section we will: \* Run Seurat DE \* Compare results of the different methods.

Load data
---------

First read in the data

``` r
# we will read both counts and rpkms as different method are more adopted for different type of data
R<-read.table("data/mouse_embryo/rpkms_Deng2014_preinplantation.txt")
M <- read.table("data/mouse_embryo/Metadata_mouse_embryo.csv",sep=",",header=T)

# select only 8 and 16 stage embryos.
selection <- c(grep("8cell",M$Stage),grep("16cell",M$Stage))

# select those cells only from the data frames
M<-M[selection,]
R <- R[,selection]
```

Seurat DE tests
===============

Seurat has four tests for differential expression (DE) which can be set with the test.use parameter in the FindMarkers() function:

-   ROC test (roc)
-   t-test (t)
-   LRT test based on zero-inflated data (bimod)
-   LRT test based on tobit-censoring models (tobit)

``` r
library(Seurat)

data <- CreateSeuratObject(raw.data = R, min.cells = 3, min.genes = 200, project = "ILC",is.expr=1,meta.data=M)

# Normalize the data
scale.factor <- mean(colSums(R))
data <- NormalizeData(object = data, normalization.method = "LogNormalize",  scale.factor = scale.factor)

# regress out number of detected genes.d
data <- ScaleData(object = data, vars.to.regress = c("nGene"),display.progress = F)

# check that the seurat identity is set to the stages
head(data@ident)
```

    ## X8cell_1.1 X8cell_1.2 X8cell_1.4 X8cell_1.5 X8cell_1.6 X8cell_1.7 
    ##     X8cell     X8cell     X8cell     X8cell     X8cell     X8cell 
    ## Levels: X16cell X8cell

``` r
# run all DE methods
methods <- c("bimod","roc","t","tobit")
DE <- list()
for (m in methods){
    outfile <- paste("data/mouse_embryo/DE/seurat_",m,"_8cell_vs_16_cell.tab", sep='')
    if(!file.exists(outfile)){
      DE[[m]]<- FindMarkers(object = data,ident.1 = "X8cell",
                        ident.2 = "X16cell",test.use = m)
      write.table(DE[[m]],file=outfile,sep="\t",quote=F)
    }
}
```

Summary of results.
===================

### Read in all the data

``` r
DE <- list()
files <- c("data/mouse_embryo/DE/sc3_kwtest_8cell_vs_16_cell.tab","data/mouse_embryo/DE/scde_8cell_vs_16_cell.tab","data/mouse_embryo/DE/seurat_bimod_8cell_vs_16_cell.tab","data/mouse_embryo/DE/seurat_roc_8cell_vs_16_cell.tab","data/mouse_embryo/DE/seurat_t_8cell_vs_16_cell.tab","data/mouse_embryo/DE/seurat_tobit_8cell_vs_16_cell.tab")
for (i in 1:6){ 
  DE[[i]]<-read.table(files[i],sep="\t",header=T)
}
names(DE)<-c("SC3","SCDE","seurat-bimod","seurat-roc","seurat-t","seurat-tobit")

# MAST file has gene names as first column, read in separately
DE$MAST <- read.table("data/mouse_embryo/DE/mast_8cell_vs_16_cell.tab", sep="\t",header=T,row.names=2)

# get top 100 genes for each test
top.100 <- lapply(DE,function(x) rownames(x)[1:100])

# load a function for plotting overlap
source("data/mouse_embryo/DE/overlap_phyper.R")
# plot overlap and calculate significance with phyper test, as background, set number of genes in seurat.
o <- overlap_phyper(top.100,plot=T,bg=nrow(DE$`seurat-bimod`))
```

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-3-1.png) Rows and columns are the different gene lists, and in the upper triangle the comparison of 2 datasets is shown with number of genes in common and color according to significance of overlap. Last columns show number of unique genes per list.

### Significant DE genes

``` r
# the Seurat p-values does not seem to be adjusted, so we need to adjust them first.
adjust <- c(3,5,6)
DE[adjust] <- lapply(DE[adjust], function(x) cbind(x,p.adjust(x$p_val)))


# not really a p-value for the ROC test, so skip for now (4th entry in the list)
pval.column <- c(2,8,5,5,5,6) # define which column contains the p-value
names(pval.column)<-names(DE)[-4]
sign.genes <- list()
cutP<-0.05
for (n in names(DE)[-4]){
  sg <- which(DE[[n]][,pval.column[n]] < cutP)
  sign.genes[[n]]<-rownames(DE[[n]])[sg]
}

# number of genes per dataset
unlist(lapply(sign.genes,length))
```

    ##          SC3         SCDE seurat-bimod     seurat-t seurat-tobit 
    ##           31           41          230           46           32 
    ##         MAST 
    ##           59

``` r
# check overlap again
o <- overlap_phyper(sign.genes,plot=T,bg=nrow(DE$`seurat-bimod`))
```

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-4-1.png)

``` r
# list genes found in all 6 methods
t<-table(unlist(sign.genes))
head(sort(t,decreasing=T),n=10)
```

    ## 
    ##  Ly6g6e    Odc1   Socs3   Cox8a Gm15645    Idua   Kank4   Klf17   Tcf20 
    ##       6       6       6       5       5       5       5       5       5 
    ## Gpatch1 
    ##       4

Only 3 genes detected by all 6 methods as DE.

### Plot top DE genes onto tSNE

``` r
# run a tsne for plotting onto
data <- FindVariableGenes(object = data, mean.function = ExpMean, dispersion.function = LogVMR, x.low.cutoff = 0.2, x.high.cutoff = 10, y.cutoff = 0.5)
```

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-5-1.png)

``` r
data <- RunPCA(object = data,do.print=F)
data <- RunTSNE(object = data, dims.use = 1:7, do.fast = TRUE,perplexity=10)
# plot first with color by celltype
TSNEPlot(data)
```

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-5-2.png)

``` r
# plot top 9 genes for each method
for (n in names(sign.genes)){
  print(n)
  p <- FeaturePlot(object = data, features.plot = sign.genes[[n]][1:9],  reduction.use = "tsne",no.axes=T,do.return = T)
}
```

    ## [1] "SC3"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-5-3.png)

    ## [1] "SCDE"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-5-4.png)

    ## [1] "seurat-bimod"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-5-5.png)

    ## [1] "seurat-t"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-5-6.png)

    ## [1] "seurat-tobit"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-5-7.png)

    ## [1] "MAST"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-5-8.png)

### Violin plots with the top genes

``` r
# plot top 9 genes for each method
for (n in names(sign.genes)){
  print(n)
  p2 <- VlnPlot(object = data, features.plot = sign.genes[[n]][1:9], nCol=3,do.return = T,size.x.use = 7, size.y.use = 7, size.title.use = 10,)
  print(p2)
}
```

    ## [1] "SC3"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-6-1.png)

    ## [1] "SCDE"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-6-2.png)

    ## [1] "seurat-bimod"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-6-3.png)

    ## [1] "seurat-t"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-6-4.png)

    ## [1] "seurat-tobit"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-6-5.png)

    ## [1] "MAST"

![](Differential_gene_expression_part2_files/figure-markdown_github/unnamed-chunk-6-6.png)

You can judge for yourself which of the methods you feel is performing best, there is no true gold standard.
