Analysis of data using Seurat package, following tutorial at: <http://satijalab.org/seurat/pbmc3k_tutorial.html>

We recommend that you follow steps XX in the tutorial.

Seurat has specific functions for loading and working with drop-seq data. Please use the provided dataset for PBMCs that comes with the tutorial. But you can also run with your own data or with the example data, but keep in mind that may functions assume that the count data is UMIs. Below is an example with human innate lympoid cells (ILCs) from Bjorklund et al. 2016.

### Load packages

``` r
suppressMessages(require(Seurat))
suppressMessages(require(gridExtra))
```

### Load expression values and metadata

``` r
R <- read.table("data/ILC/ensembl_rpkmvalues_ILC.csv",sep=",",header=T,row.names=1)
M <- read.table("data/ILC/Metadata_ILC.csv",sep=",",header=T,row.names=1)

# in this case it may be wise to translate ensembl IDs to gene names to make plots with genes more understandable
TR <- read.table("data/ILC/gene_name_translation_biotype.tab",sep="\t")

# find the correct entries in TR and merge ensembl name and gene id.
m <- match(rownames(R),TR$ensembl_gene_id)
newnames <- apply(cbind(as.vector(TR$external_gene_name)[m],rownames(R)),1,paste,collapse=":")
rownames(R)<-newnames

# check which genes are spike-ins in the matrix
spikes <- grepl("ERCC_",rownames(R))

celltype2cols <- c("blue", "cyan", "red", "green")[as.integer(M$Celltype)]
donor2cols <- c("black", "orange", "magenta")[as.integer(M$Donor)]
```

### Create seurat object

Will automatically filter out genes/cells that do not meet the criteria specified to save space.

``` r
ercc <- grep("ERCC_",rownames(R))

data <- CreateSeuratObject(raw.data = R[-ercc,], min.cells = 3, min.genes = 200, 
    project = "ILC",is.expr=1,meta.data=M)

# plot number of genes and nUMI (rpkms in this case) for each Donor
VlnPlot(object = data, features.plot = c("nGene", "nUMI"), nCol = 2)
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-3-1.png)

``` r
# same for celltype
VlnPlot(object = data, features.plot = c("nGene", "nUMI"), nCol = 2,group.by="Celltype")
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-3-2.png)

``` r
# scatterplot with detected genes vs UMIs
GenePlot(object = data, gene1 = "nUMI", gene2 = "nGene")
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-3-3.png)

``` r
# the slot data@ident defines the classes of cells, which is automatically set to plate id, To instead plot by celltype, data@ident needs to be changed.
data <- SetAllIdent(object = data, id = "Celltype")
GenePlot(object = data, gene1 = "nUMI", gene2 = "nGene")
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-3-4.png)

``` r
# change ident back to Donor
data <- SetAllIdent(object = data, id = "Donor")
```

OBS! Each time you want to change colors in a gene plot, you need to change the identity class value in the seurat object in the slot <data@ident>. Perhaps there is a better way, but I did not find a solution.

In many of the other Seurat plotting functions like TNSEPlot and PCAPlot you can use "group.by" to define which meta data variable the cells should be coloured by.

Data scaling
------------

``` r
# set scale factor according to mean library size
scale.factor <- mean(colSums(R))
data <- NormalizeData(object = data, normalization.method = "LogNormalize", 
    scale.factor = scale.factor)

# look at the plot for suitable cutoffs and rerun
data <- FindVariableGenes(object = data, mean.function = ExpMean, dispersion.function = LogVMR, x.low.cutoff = 0.2, x.high.cutoff = 10, y.cutoff = 0.5)
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-4-1.png)

``` r
length(x = data@var.genes)
```

    ## [1] 6624

``` r
# regress out number of detected genes.d
data <- ScaleData(object = data, vars.to.regress = c("nGene"),display.progress=F)

# also with batch info
dataB <- ScaleData(object = data, vars.to.regress = c("nGene","Donor"),display.progress=F)
```

PCA
---

``` r
data <- RunPCA(object = data, pc.genes = data@var.genes, do.print = TRUE, pcs.print = 1:5,  genes.print = 5)
```

    ## [1] "PC1"
    ## [1] "TYROBP:ENSG00000011600"   "FCER1G:ENSG00000158869"  
    ## [3] "LST1:ENSG00000204482"     "IL4I1:ENSG00000104951"   
    ## [5] "TNFSF13B:ENSG00000102524"
    ## [1] ""
    ## [1] "CD3G:ENSG00000160654"  "CD3D:ENSG00000167286"  "IL32:ENSG00000008517" 
    ## [4] "TRAT1:ENSG00000163519" "SELL:ENSG00000188404" 
    ## [1] ""
    ## [1] ""
    ## [1] "PC2"
    ## [1] "NKG7:ENSG00000105374"  "KLRD1:ENSG00000134539" "KLRF1:ENSG00000150045"
    ## [4] "PRF1:ENSG00000180644"  "CST7:ENSG00000077984" 
    ## [1] ""
    ## [1] "IL4I1:ENSG00000104951"   "JAML:ENSG00000160593"   
    ## [3] "CD200R1:ENSG00000163606" "KRT1:ENSG00000167768"   
    ## [5] "HPGDS:ENSG00000163106"  
    ## [1] ""
    ## [1] ""
    ## [1] "PC3"
    ## [1] "RNU2-59P:ENSG00000222414" "KRT1:ENSG00000167768"    
    ## [3] "HPGDS:ENSG00000163106"    "RNU2-61P:ENSG00000223001"
    ## [5] "DUSP1:ENSG00000120129"   
    ## [1] ""
    ## [1] "CD3G:ENSG00000160654"       "CD3D:ENSG00000167286"      
    ## [3] "AC010970.1:ENSG00000225840" "CD2:ENSG00000116824"       
    ## [5] "CD27:ENSG00000139193"      
    ## [1] ""
    ## [1] ""
    ## [1] "PC4"
    ## [1] "NA:ENSG00000265150"    "PRF1:ENSG00000180644"  "NKG7:ENSG00000105374" 
    ## [4] "HOPX:ENSG00000171476"  "ZFP36:ENSG00000128016"
    ## [1] ""
    ## [1] "IGHM:ENSG00000211899"  "BLNK:ENSG00000095585"  "CD19:ENSG00000177455" 
    ## [4] "NA:ENSG00000260655"    "NAPSB:ENSG00000131401"
    ## [1] ""
    ## [1] ""
    ## [1] "PC5"
    ## [1] "RNVU1-14:ENSG00000207501" "FOS:ENSG00000170345"     
    ## [3] "DUSP1:ENSG00000120129"    "RNVU1-6:ENSG00000201558" 
    ## [5] "NA:ENSG00000202364"      
    ## [1] ""
    ## [1] "CDCA7:ENSG00000144354"      "UBE2T:ENSG00000077152"     
    ## [3] "KIF15:ENSG00000163808"      "CENPW:ENSG00000203760"     
    ## [5] "AL355336.1:ENSG00000225092"
    ## [1] ""
    ## [1] ""

``` r
dataB <- RunPCA(object = dataB, pc.genes = data@var.genes, do.print = TRUE, pcs.print = 1:5,  genes.print = 5)
```

    ## [1] "PC1"
    ## [1] "CD3G:ENSG00000160654"  "CD3D:ENSG00000167286"  "IL32:ENSG00000008517" 
    ## [4] "SELL:ENSG00000188404"  "TRAT1:ENSG00000163519"
    ## [1] ""
    ## [1] "TYROBP:ENSG00000011600"   "LST1:ENSG00000204482"    
    ## [3] "IL4I1:ENSG00000104951"    "FCER1G:ENSG00000158869"  
    ## [5] "TNFSF13B:ENSG00000102524"
    ## [1] ""
    ## [1] ""
    ## [1] "PC2"
    ## [1] "JAML:ENSG00000160593"    "LTB:ENSG00000227507"    
    ## [3] "IL4I1:ENSG00000104951"   "S100A4:ENSG00000196154" 
    ## [5] "CD200R1:ENSG00000163606"
    ## [1] ""
    ## [1] "NKG7:ENSG00000105374"  "KLRD1:ENSG00000134539" "KLRF1:ENSG00000150045"
    ## [4] "PRF1:ENSG00000180644"  "GZMK:ENSG00000113088" 
    ## [1] ""
    ## [1] ""
    ## [1] "PC3"
    ## [1] "CD3G:ENSG00000160654"       "AC010970.1:ENSG00000225840"
    ## [3] "CD3D:ENSG00000167286"       "CD2:ENSG00000116824"       
    ## [5] "CD27:ENSG00000139193"      
    ## [1] ""
    ## [1] "KRT1:ENSG00000167768"     "HPGDS:ENSG00000163106"   
    ## [3] "IL17RB:ENSG00000056736"   "PKIB:ENSG00000135549"    
    ## [5] "RNU2-59P:ENSG00000222414"
    ## [1] ""
    ## [1] ""
    ## [1] "PC4"
    ## [1] "NKG7:ENSG00000105374"  "PRF1:ENSG00000180644"  "KLRD1:ENSG00000134539"
    ## [4] "CST7:ENSG00000077984"  "ZFP36:ENSG00000128016"
    ## [1] ""
    ## [1] "IGHM:ENSG00000211899"  "BLNK:ENSG00000095585"  "CD19:ENSG00000177455" 
    ## [4] "NAPSB:ENSG00000131401" "NA:ENSG00000260655"   
    ## [1] ""
    ## [1] ""
    ## [1] "PC5"
    ## [1] "NA:ENSG00000265150"       "MBOAT2:ENSG00000143797"  
    ## [3] "RNVU1-14:ENSG00000207501" "SLAMF1:ENSG00000117090"  
    ## [5] "IL1RL1:ENSG00000115602"  
    ## [1] ""
    ## [1] "UBE2T:ENSG00000077152" "CDCA7:ENSG00000144354" "KIF15:ENSG00000163808"
    ## [4] "NA:ENSG00000196532"    "CENPW:ENSG00000203760"
    ## [1] ""
    ## [1] ""

``` r
# plot gene loadings
VizPCA(object = data, pcs.use = 1:4)
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-5-1.png)

``` r
# Plot pca for both normalizatinos, since we want to put multiple plots in the same window, do.return=T was used to return the ggplot2 objec.t
p1 <- PCAPlot(object = data, dim.1 = 1, dim.2 = 2, do.return=T)
p2 <- PCAPlot(object = dataB, dim.1 = 1, dim.2 = 2, do.return=T)

# and with both color by Celltype, here you can use group.by 
p3 <- PCAPlot(object = data, dim.1 = 1, dim.2 = 2, do.return=T,group.by="Celltype")
p4 <- PCAPlot(object = dataB, dim.1 = 1, dim.2 = 2, do.return=T,group.by="Celltype")

# plot together
grid.arrange(p1,p2,p3,p4,ncol=2)
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-5-2.png)

``` r
# heatmap with top loading genes
# OBS! margins too large to display in R-studio, save to pdf instead.
pdf("data/ILC/seurat_pc_loadings_heatmaps.pdf")
PCHeatmap(object = data, pc.use = 1, do.balanced = TRUE, label.columns = FALSE)
PCHeatmap(object = data, pc.use = 1:5, do.balanced = TRUE, label.columns = FALSE)
dev.off()
```

    ## quartz_off_screen 
    ##                 2

Determine statistically significant principal components
--------------------------------------------------------

If dataset is large, instead use `PCElbowPlot()`.

``` r
data <- JackStraw(object = data, num.replicate = 100, do.print = FALSE)
JackStrawPlot(object = data, PCs = 1:12)
```

    ## Warning: Removed 64731 rows containing missing values (geom_point).

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-6-1.png)

Find clusters
-------------

In this case, we use the PCs as suggested by the JackStrawPlot.

``` r
use.pcs <- c(1,2,3,5,6,10)
data <- FindClusters(object = data, reduction.type = "pca", dims.use = use.pcs, 
    resolution = 0.6, print.output = 0, save.SNN = TRUE)

PrintFindClustersParams(object = data)
```

    ## Parameters used in latest FindClusters calculation run on: 2018-01-19 11:45:00
    ## =============================================================================
    ## Resolution: 0.6
    ## -----------------------------------------------------------------------------
    ## Modularity Function    Algorithm         n.start         n.iter
    ##      1                   1                 100             10
    ## -----------------------------------------------------------------------------
    ## Reduction used          k.param          k.scale          prune.SNN
    ##      pca                 30                25              0.0667
    ## -----------------------------------------------------------------------------
    ## Dims used in calculation
    ## =============================================================================
    ## 1 2 3 5 6 10

tSNE
----

``` r
data <- RunTSNE(object = data, dims.use = use.pcs, do.fast = TRUE)

# note that you can set do.label=T to help label individual clusters
TSNEPlot(object = data,do.label = T)
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-8-1.png)

``` r
# compare to celltype identities, colour instead by Celltype with group.by
TSNEPlot(object = data, group.by = "Celltype")
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-8-2.png) Color now automatically changes to the cluster identities.

### Cluster markers

Now we can plot some of the cluster markers

``` r
# find all genes that defines cluster1
cluster1.markers <- FindMarkers(object = data, ident.1 = 1, min.pct = 0.25)
print(x = head(x = cluster1.markers, n = 5))
```

    ##                               p_val  avg_diff pct.1 pct.2
    ## CD2:ENSG00000116824    1.136731e-98 -6.123346 0.204 0.943
    ## SCN2A:ENSG00000136531  7.981167e-79  1.394704 0.373 0.304
    ## HPGDS:ENSG00000163106  2.192176e-68  5.583182 0.634 0.014
    ## IL32:ENSG00000008517   2.412807e-65  1.368349 0.993 0.692
    ## SH2D1B:ENSG00000198574 3.165102e-55 -5.566821 0.239 0.688

``` r
# plot top cluster1 markers
VlnPlot(object = data, features.plot = rownames(cluster1.markers)[1:6],nCol=3,size.title.use=10)
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-9-1.png)

``` r
# or plot them onto tSNE
FeaturePlot(object = data, features.plot = rownames(cluster1.markers)[1:6], cols.use = c("grey", "blue"), reduction.use = "tsne")
```

![](seurat_analysis_files/figure-markdown_github/unnamed-chunk-9-2.png)

``` r
# find genes that separates cluster 0 & cluster 3
cluster03.markers <- FindMarkers(object = data, ident.1 = 0, ident.2 = 3, min.pct = 0.25)
print(x = head(x = cluster03.markers, n = 5))
```

    ##                                p_val   avg_diff pct.1 pct.2
    ## FOS:ENSG00000170345     2.841879e-52 -1.2924143 1.000 1.000
    ## DUSP1:ENSG00000120129   1.218415e-39 -0.8025687 0.995 1.000
    ## TSC22D3:ENSG00000157514 6.780012e-33 -0.6966452 0.986 1.000
    ## JUND:ENSG00000130522    1.709752e-27 -0.7250719 0.828 0.990
    ## NA:ENSG00000212544      1.053706e-25 -1.9166617 0.109 0.714
